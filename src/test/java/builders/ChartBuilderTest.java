package builders;

import models.*;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;

public class BuilderTest {



    @Test
    public SetType() {
        ChartBuilder chartBuilder = new ChartBuilder();
        ChartType testChartType = ChartType.Line;

        ChartSettings chartSettings = chartBuilder.withType(testChartType)
                .build();

        assertThat(chartSettings.getChartType()).isEqualTo(testChartType);
    }
    
    @Test
    public void SetHaveLegendToTrue() {
        ChartBuilder chartBuilder = new ChartBuilder();

        ChartSettings chartSettings = chartBuilder.withLegend()
                .build();

        assertThat(chartSettings.isHaveLegend()).isTrue();
    }

	   @Test
	    public void SetTitle() {
	        ChartBuilder chartBuilder = new ChartBuilder();
	        String testTitlle = "SuperTitle";

	        ChartSettings chartSettings = chartBuilder.withTitle(testTitlle)
	                .build();

	        assertThat(chartSettings.getTitle()).isEqualTo(testTitlle);
	    }
	
	@Test
    public void SetChartSeries() {
        ChartBuilder chartBuilder = new ChartBuilder();
        ChartSerie chartSerie1 = new ChartSerie("aloha",
                Arrays.asList(new Point(71, 2), new Point(52, 14), new Point(55, 44)),
                SerieType.LinePoint);

        ChartSettings chartSettings = chartBuilder.withSeries(Collections.singletonList(chartSerie1))
                .build();

        assertThat(chartSettings.getSeries()).hasSize(1);
        assertThat(chartSettings.getSeries()).contains(chartSerie1);
    }

 

    @Test
    public void SetSubtitle() {
        ChartBuilder chartBuilder = new ChartBuilder();
        String TestSub = "nextSubtitle";

        ChartSettings chartSettings = chartBuilder.withSubtitle(TestSub)
                .build();

        assertThat(chartSettings.getSubtitle()).isEqualTo(TestSub);
    }



    @Test
    public void CorrectChartSettings() {
        ChartBuilder chartBuilder = new ChartBuilder();
        String testTitlle = "Titlle";
        String TestSub = "Subtitle2";
        ChartType testChartType = ChartType.Bar;

        ChartSettings chartSettings = chartBuilder.withTitle(testTitlle)
                .withSubtitle(TestSub)
                .withType(testChartType)
                .withLegend()
                .build();

        assertThat(chartSettings.getTitle()).isEqualTo(testTitlle);
        assertThat(chartSettings.getSubtitle()).isEqualTo(TestSub);
        assertThat(chartSettings.getChartType()).isEqualTo(testChartType);
        assertThat(chartSettings.isHaveLegend()).isTrue();
    }

    @Test
    public void ReturnChartSettingsAsFalse_When_WithLegendIsNotUsed() {
        ChartBuilder chartBuilder = new ChartBuilder();
        String testTitlle = "Titlle";
        String TestSub = "Subtitle2";

        ChartSettings chartSettings = chartBuilder.withTitle(TestTitle)
                .withSubtitle(TestSub)
                .build();

        assertThat(chartSettings.isHaveLegend()).isFalse();
    }
}
